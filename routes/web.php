<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index');
Route::get('/aboutUs', 'AboutController@index');
Route::get('/costumerProfile', 'CostumerProfileController@index');
Route::get('/ourFactory', 'FactoryController@index');
Route::get('/gallery', 'GalleryController@index');
Route::get('/contactUs', 'ContactUsController@index');
Route::get('/news', 'NewsController@index');
Route::get('/news/{id}', 'NewsController@detailNews');

Route::post('/send-message', 'ContactUsController@sendMessage');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin', 'Admin\NewsPostController@index');

Route::get('/admin/news', 'Admin\NewsPostController@masterNews');
Route::get('/admin/add-news', 'Admin\NewsPostController@formNews');
Route::post('/admin/publish-news', 'Admin\NewsPostController@publishNews');
Route::get('/admin/delete-news/{id}', 'Admin\NewsPostController@deleteNews');
Route::get('/admin/edit-news/{id}', 'Admin\NewsPostController@formEditNews');
Route::post('/admin/update-news/{id}', 'Admin\NewsPostController@editNews');

Route::get('/admin/messages','Admin\MessagesController@index');
Route::get('/admin/show-message/{id}','Admin\MessagesController@showMessage');