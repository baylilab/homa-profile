@extends("front.main")
@section('pages', $pages)
@section('title', $title)
@section("costumer-profile")
<div class="header-banner">
        <h2>Costumer Profile</h2>
</div>
<div class="row costumer-profile-1">
<div class="col-md-2"></div>
    <div class="col-md-8">
        <p>
            As years goes by, HOMA becomes one of the most established manufacturer for Modern Market  Retail, Project and export customers.  We have served many of our customers in parts or in whole  projects.
        </p>
    </div>
    <div class="col-md-2"></div>
</div>

<div class="row costumer-profile-2">

<div class="col-md-2"></div>
    <div class="col-md-4 left">
    <p><b>RETAILS</b> costumers :</p>
        <ul>
            <li>Lotemart</li>
            <li>Pongs</li>
            <li>Atria</li>
            <li>Depo</li>
            <li>Ajbs</li>
            <li>Home Solution</li>
            <li>Abdi Jaya</li>
            <li>New Home Center</li>
            <li>Dynasti</li>
            <li>Wira Kencana</li>
        </ul>
    </div>
    <div class="col-md-4 right">
         <ul>
            <li>Rumah kita</li>
            <li>Kompas</li>
            <li>Archie Galery Furniture</li>
            <li>Bintang Timur</li>
            <li>Subur</li>
            <li>Panca Putra</li>
            <li>Carson</li>
            <li>Kemenangan jaya</li>
            <li>Depo Pelita</li>
            <li>Jakarta furniture and many more..</li>
        </ul>
    </div>
    <div class="col-md-2"></div>

</div>

<div class="row costumer-profile-2">

<div class="col-md-2"></div>
    <div class="col-md-4 left">
    <p><b>HOTELS</b> costumers :</p>
        <ul>
            <li>Grand Royal Panghegar</li>
            <li>Hotel Permata – Krakatau Steel Group </li>
            <li>Skyland Condotel</li>
            <li>Hotel Bali World</li>
            <li>The Hive</li>
            <li>The Lagoon</li>
            <li>Grand Malabar</li>
            <li>Formula 1 Hotel</li>
        </ul>
    </div>
    <div class="col-md-4 right">
         <ul>
            <li>Raddison Hotel</li>
            <li>Manhatan Hotel</li>
            <li>Hotel Horizzon</li>
            <li>Hotel Zodiak</li>
            <li>Santika Group</li>
            <li>Hotel Amaris</li>
            <li>Agung sedayu Group</li>
            <li>Hotel Garuda Plaza And many more</li>
        </ul>
    </div>
    <div class="col-md-2"></div>

</div>
<div class="row costumer-profile-2">

<div class="col-md-2"></div>
    <div class="col-md-4 left">
    <p><b>RESTON and CAFFE</b> costumers :</p>
        <ul>
            <li>Kentucky Fried Chicken(KFC)</li>
            <li>Chef Yang Restaurant</li>
            <li>Galael Caffe</li>
            <li>Hotel Bali World</li>
            <li>Gokana</li>
            <li>Bakso Malang Karapitan</li>
            <li>Platinum</li>
            <li>D'Cost</li>
            <li>Hero</li>
            <li>Bakmi Gading</li>
        </ul>
    </div>
    <div class="col-md-4 right">
         <ul>
            <li>Bakmi Gajah Mada</li>
            <li>Read Bean</li>
            <li>Thain Express</li>
            <li>AW</li>
            <li>Doan Vietnam Restaurant</li>
            <li>Food Court Pasar raya</li>
            <li>Food Court Mall Taman Palm</li>
            <li>Food Court Mall SKA</li>
            <li>Food Court Mall Lucky Square</li>
        </ul>
    </div>
    <div class="col-md-2"></div>

</div>


<div class="row costumer-profile-2">

<div class="col-md-2"></div>
    <div class="col-md-4 left">
    <p><b>OFFICE</b> costumers :</p>
        <ul>
            <li>Krakatau Steel</li>
            <li>Rico Bana</li>
            <li>Merla Sakti Abadi</li>
            <li>Karya Cipta Putra Persada</li>
            <li>Karya Cipta Pratama</li>
            <li>Karya Cipta Prestasi</li>
            <li>Super Makmus</li>
            <li>Super Exim Sari and Many more..</li>
        </ul>
    </div>
    <div class="col-md-2"></div>

</div>
<div class="row costumer-profile-2">

<div class="col-md-2"></div>
    <div class="col-md-4 left">
    <p><b>HOSPITAL</b> costumers :</p>
        <ul>
            <li>RSCM</li>
            <li>RS. Saint Carolous</li>
            <li>Rumah sakit Islam</li>
            <li>RS ani</li>
            <li>Clinic Erpour</li>
        </ul>
    </div>
    <div class="col-md-2"></div>

</div>

<div class="row costumer-profile-2">

<div class="col-md-2"></div>
    <div class="col-md-4 left">
    <p><b>EDUCATION</b> costumers :</p>
        <ul>
            <li>UNTAR</li>
            <li>English First</li>
        </ul>
    </div>
    <div class="col-md-2"></div>

</div>

@endsection