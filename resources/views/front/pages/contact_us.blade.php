@extends("front.main")
@section('pages', $pages)
@section('title', $title)
@section("contact-us")
<div class="header-banner">
    <h2>Contact us</h2>
</div>
<div class="content">
<div class="contact">
    <div class="contact-map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7934.566018646978!2d106.71128031767967!3d-6.0925271846753155!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e6a03256fc44093%3A0xeee9ad88e5e0466f!2sHoma+Sejahtera.+PT!5e0!3m2!1sid!2sid!4v1520662249275" width="100%" height="151" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <div class="contact_top">
         
             <div class="col-md-8 contact_left">
                 <h3>Contact Form</h3>
                 <p>For business purposes, you can send us a message through the form below :</p>
                 @if(Session::has('success'))  
                 <div class="alert alert-success">
                   {{ Session::get('success')}} 
                 </div>
                 @endif
                  <form action="send-message" method="POST">
                      {{ csrf_field() }}
                     <div class="form_details">
                         <input type="text" class="text" name="name" placeholder="Your Name">
                         <input type="text" class="text" name="email" placeholder="Your Email">
                         <input type="text" class="text" name="subject" placeholder="Subject">
                         <textarea name="message" placeholder="Message"></textarea>
                         <div class="clearfix"> </div>
                         <div class="sub-button">
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                             <input type="submit" value="Send message" name="send_message">
                         </div>
                      </div>
                   </form>
                   
                </div>
                <div class="col-md-4 company-right">
                    <div class="company_ad">
                             <h3>Contact Info</h3>
                          
                              <address>
                                 <p>Email:<a href="mailto:sales@homasejahtera.co.id">sales@homasejahtera.co.id</a></p>
                                 <p>Phone: 1.306.222.4545</p>
                                   <p>Pergudangan Parung Harapan Blok BF/9, JL Perancis, Dadap, Tangerang, Banten 15125</p>
                                   
                                  
                               </address>
                           </div>
                       </div>	
                <div class="clearfix"> </div>
        </div>
    </div>
</div>
@endsection