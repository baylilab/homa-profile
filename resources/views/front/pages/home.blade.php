@extends("front.main")
@section('pages', $pages)
@section('title', $title)
@section("home")
<div class="fluid_container">
        <div class="camera_wrap camera_azure_skin" id="camera_wrap_1">
            <div data-thumb="{{ asset('asset/images/slider1.jpg') }}" data-src="{{ asset('asset/images/slider1.jpg') }}"></div>
            <div data-thumb="{{ asset('asset/images/slider2.jpg') }}" data-src="{{ asset('asset/images/slider2.jpg') }}"></div>
            <div data-thumb="{{ asset('asset/images/slider4.jpg') }}" data-src="{{ asset('asset/images/slider4.jpg') }}"></div>
            <div data-thumb="{{ asset('asset/images/slider5.jpg') }}" data-src="{{ asset('asset/images/slider5.jpg') }}"></div>            
        </div>
        <!-- #camera_wrap_1 -->
        <div class="clearfix"></div>
    </div>
    <div class="interior-grids">
        <div class="col-md-4 interior-grid">
            <h3>COMPANY PROFILE</h3>
            <div class="gallery">
                <a href="{{ asset('asset/images/pic1.jpg') }}"><img src="{{ asset('asset/images/pic1.jpg') }}" class="img-responsive" alt="/" title="image-name"></a>
            </div>		
            <p>PT. HOMA SEJAHTERA (HOMA) was established in 2001 May 25 under the notarial deed no.75. The company domicile in Tangerang regency. </p>
            <div class="plus_btn">
                <a href="/aboutUs"><span></span></a>
            </div>
        </div>
        <div class="col-md-4 interior-grid">
            <h3>CUSTOMER PROFILE</h3>
            <div class="gallery">
                <a href="{{ asset('asset/images/pic2.jpg') }}"><img src="{{ asset('asset/images/pic2.jpg') }}" class="img-responsive" alt="/" title="image-name"></a>
            </div>		
            <p>As years goes by, HOMA becomes one of the most established manufacturer for Modern Market
Retail, Project and export customers.</p>
            <div class="plus_btn">
                <a href="/costumerProfile"><span></span></a>
            </div>
        </div>
        <div class="col-md-4 interior-grid">
            <h3>OUR FACTORY</h3>
            <div class="gallery">
                <a href="{{ asset('asset/images/pic3.jpg') }}"><img src="{{ asset('asset/images/pic3.jpg') }}" class="img-responsive" alt="/" title="image-name"></a>
            </div>		
            <p>HOMA used state of the art panel saw from Italy Biesse, CNC programmed and a heavy duty panel saw to ensure maximum capacity.</p>
            <div class="plus_btn">
                <a href="/ourFactory"><span></span></a>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="product-section">
        <h3>Our Costumer</h3>
    <div class="product-grids">
            <div class="col-md-3 product-grid">
            <a href="/gallery" class="mask">
         <img src="{{ asset('asset/images/e1.jpg') }}" class="img-responsive zoom-img" alt="">
        </a>
            </div>
            <div class="col-md-3 product-grid1">
                <h4><a href="/gallery">Grand Malabar Hotel</a></h4>
                <p>GRAND PERMATA HOTEL
Owned and operated by Krakatau
Steel in Cilegon. A modern and
minimalist hotel furnished by HOMA.</p>
                <a href="/gallery" class="button1">more</a>
            </div>
            <div class="col-md-3 product-grid">
            <a href="/gallery" class="mask">
         <img src="{{ asset('asset/images/e2.jpg') }}" class="img-responsive zoom-img" alt="">
        </a>
            </div>
            <div class="col-md-3 product-grid1">
            <h4><a href="/gallery">Grand Royal Panghegar</a> </h4>
                <p>Grand Royal Panghegar A 5 Star hotel in Bandung prime shopping district. Fully furnished by HOMA.</p>
                <a href="/gallery" class="button1">more</a>
            </div>
            <div class="clearfix"></div>
            </div>
    </div>
    
    <div class="new-section">
    <h3>Latest News</h3>

        <div class="new-grids">

        @if(count($news)==0)
            <h4>No News Today</h4>
        @else

        @foreach($news as $row)
        <div class="col-md-6 new-grid">
        <div class="new-number">
        <h4>{!! substr($row->created_at, 5, 2) !!}</h4>
        </div>
        <div class="new-text">
        <h5>{!! substr($row->title, 0, 100) !!} </h5>
            <p>{!! substr($row->news, 0, 100) !!}.</p>	
        </div>
        <div class="clearfix"></div>
        </div>
        @endforeach
        @endif
    
        <div class="clearfix"></div>
</div>
</div>
@endsection