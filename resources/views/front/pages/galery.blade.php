@extends("front.main")
@section('pages', $pages)
@section('title', $title)
@section("gallery")
<div class="header-banner">
        <h2>Gallery</h2>
</div>
<div class="row costumer-profile-2">
<div class="col-md-2"></div>
    <div class="col-md-8">
        <img src="{{ asset('asset/images/galery1.png') }}" class="img-responsive" alt="">
        <img src="{{ asset('asset/images/galery2.png') }}" class="img-responsive" alt="">
        <img src="{{ asset('asset/images/galery3.png') }}" class="img-responsive" alt="">
        <img src="{{ asset('asset/images/galery4.png') }}" class="img-responsive" alt="">
        <img src="{{ asset('asset/images/galery5.png') }}" class="img-responsive" alt="">
    </div>
    <div class="col-md-2"></div>

</div>
@endsection