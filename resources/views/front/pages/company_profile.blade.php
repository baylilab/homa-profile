<p>PT. HOMA SEJAHTERA (HOMA) was established in 2001 May 25 under the notarial deed no.  75. The company domicile in Tangerang regency in Kosambi area.</p>
<p>
    Since its establishment, HOMA concentrate to expertise itself in wood working manufacturing.  HOMA utilized Germany and Italian machineries to ensure maximum production capacity and  European quality. HOMA used only good quality components from reliable manufacturers such as,  Rehau, Doellken, Schattdecor, Hafele, Hettich, Rakol, Jowat and many more. 
</p>
<p>
    HOMA  manufactures  panel  and  knock  down  furniture  ranging  from  MFC  (Melamine  Faced  Chipboard), HPL, PVC (Poly Vinyl Chloride), Paper laminate and many more. Homa also produces in  house brand and OEM for our customers in their own perspective brands.  HOMA grew and  established with a deep company culture and team work.  

</p>
<p>We have built our business on the core principle of providing our customers with quality products  and  creating  value‐added  services  to  our  customer,  at  competitive  pricing.  Continuous  improvements, to enhance competitiveness are a daily routine at HOMA. We see our customers as  our partner. This is a vital element in our development</p>
<span>HOMA is commited  to :</sapan>
<ul>
    <li>Maintaining customer satistaction</li>
    <li>Long term commitment and partnership</li>
    <li>Total Quality Control (TQC)</li>
    <li>On Time delivery</li>
    <li>Continuos R&D Services</li>
    <li>Low MOQ manufacturing techniques</li>
</ul>
<p>HOMA has proven records of loyal suppliers and loyal customers, be there local or international  suppliers or customers. Trust is our reputation and our foundation</p> 
