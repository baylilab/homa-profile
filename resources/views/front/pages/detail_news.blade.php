@extends("front.main")
@section('pages', $pages)
@section('title', $title)
@section("detail-news")


<div class="row">
        <div class="col-md-2"></div>
    <div class="col-md-8">
            <div class="card mb-4">
                    <img class="card-img-top img-responsive" src="{{ asset('asset/image_news') }}/{{ $news->images }}"  alt="Card image cap">
                    <div class="card-body">
                      <h2 class="card-title">{{ $news->title }}</h2>
                      <p class="card-text">{!! $news->news !!}</p>
                      
                    </div>
                    <div class="card-footer text-muted">
                      Posted on {{ Str::limit($news->created_at, 10, '') }} by
                      <a href="#">Administrator</a>
                    </div>
                  </div>
    </div>
    <div class="col-md-2"></div>
    
</div>
@endsection

