@extends("front.main")
@section('pages', $pages)
@section('title', $title)
@section("news")

@foreach($news as $row)
<div class="row">
        <div class="col-md-2"></div>
    <div class="col-md-8">
            <div class="card mb-4">
                    <img class="card-img-top img-responsive" src="{{ asset('asset/image_news') }}/{{ $row->images }}"  alt="Card image cap">
                    <div class="card-body">
                      <h2 class="card-title">{{ $row->title }}</h2>
                      <p class="card-text">{!! Str::limit($row->news, $limit = 300, $end = '...') !!}</p>
                      <a href="/news/{{ $row->id }}" class="btn btn-default">Read More →</a>
                    </div>
                    <div class="card-footer text-muted">
                      Posted on {{ Str::limit($row->created_at, 10, '') }} by
                      <a href="#">Administrator</a>
                    </div>
                  </div>
    </div>
    <div class="col-md-2"></div>
    
</div>
@endforeach
<div class="row text-center">
        <div class="col-md-2"></div>
    <div class="col-md-8">
            {{ $news->links() }}    
    </div>
    <div class="col-md-2"></div>
</div>
@endsection

