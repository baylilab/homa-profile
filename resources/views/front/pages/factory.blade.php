@extends("front.main")
@section('pages', $pages)
@section('title', $title)
@section("factory")
<div class="header-banner">
        <h2>Our Factory</h2>
</div>
<div class="row costumer-profile-2">
<div class="col-md-2"></div>
    <div class="col-md-8">
        <img src="{{ asset('asset/images/factory.png') }}" class="img-responsive" alt="">
        <img src="{{ asset('asset/images/factory2.png') }}" class="img-responsive" alt="">
    </div>
    <div class="col-md-2"></div>

</div>


@endsection