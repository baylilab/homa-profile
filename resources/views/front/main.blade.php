<!--
Au<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>@yield(@title)</title>
<link href="{{ asset('asset/css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all">
<link href="{{ asset('asset/css/style.css') }}" rel="stylesheet" type="text/css" media="all" />
<link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" media="all" />
<link href="https://fonts.googleapis.com/css?family=Abril+Fatface" rel="stylesheet">
<link rel="shortcut icon" href="{{ asset('asset/images/favicon.ico') }}" type="image/x-icon">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="{{ asset('asset/css/camera.css') }}" rel="stylesheet" type="text/css" media="all" />
    <script type='text/javascript' src='{{ asset('asset/js/jquery.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('asset/js/jquery.mobile.customized.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('asset/js/jquery.easing.1.3.js') }}'></script> 
    <script type='text/javascript' src='{{ asset('asset/js/camera.min.js') }}'></script> 
    
    <script>
		jQuery(function(){
			
			jQuery('#camera_wrap_1').camera({
				thumbnails: true
			});

			jQuery('#camera_wrap_2').camera({
				height: '400px',
				loader: 'bar',
				pagination: false,
				thumbnails: true
			});
		});
		</script>
		<!--start lightbox -->
<link rel="stylesheet" type="text/css" href="{{ asset('asset/css/jquery.lightbox.css') }}">
<script src="{{ asset('asset/js/jquery.lightbox.js') }}"></script>
<script>
  // Initiate Lightbox
  $(function() {
    $('.gallery a').lightbox(); 
  });
</script>

</head>
<body>
	<div class="container">
		<div class="main-header">
			<div class="inner-side">
				@include("front/component.header")
			<div class="logo">
						<div class="logo-border"><a href="index.html"><span>HOMA</span></a></div>
						<p>Home For Quality Product</p>	
					</div>
				</div>
				@yield($pages)
				@include("front/component.footer")
			</div>
		</div>
	</div>
	 </body>
</html>