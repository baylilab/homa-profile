<div class="header">
		<div class="head-top">
			<div class="top-menu">
			<span class="menu"><img src="{{ asset('asset/images/nav.png') }}" alt=""/> </span>
				<ul>
					<li class="active"><a href="/" onclick="active(this)"><span>Home</span></a></li>
					<li><a href="/aboutUs" onclick="active(this)" ><span>About us</span></a></li>
					<li><a href="/gallery" onclick="active(this)"><span>Galery</span></a></li>
					<li><a href="/news"><span>News</span></a></li>
					<li><a href="/contactUs"><span>Contact Us</span></a></li>
				</ul>
			</div>
			<div class="social-icons">
				<a href="#"><i class="icon1"></i></a>
				<a href="#"><i class="icon2"></i></a>
				<a href="#"><i class="icon3"></i></a>
			</div>	
			<!-- script for menu -->
						
				 <script>
				 $("span.menu").click(function(){
				 $(".top-menu ul").slideToggle("slow" , function(){
				 });
				 });
				 </script>
			<!-- //script for menu -->

			<div class="clearfix"></div>
</div>
