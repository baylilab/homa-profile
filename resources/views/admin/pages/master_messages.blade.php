@extends("admin.main")
@section('pages', $pages)
@section('title', $title)
@section("master-messages")
<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-head-line">Master Messages</h4>

            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                       
                    <div class="panel-heading">
                        @if(Session::has('deleteSuccess'))  
                                <div class="alert alert-success">
                                  {{ Session::get('deleteSuccess')}} 
                                </div>
                                @endif
                    </div>
                    
                    <div class="panel-body">
                        <div class="table-responsive">
                                
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Subject</th>
                                        <th>Tgl message</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($messages) == 0)
                                        <h3>No Data Record Found</h3>
                                    @else
                                    @foreach($messages as $row)
                                    <tr class="success">
                                        <td>{{ $row->id }}</td>
                                        <td>{{ $row->name }}</td>
                                        <td>{{ $row->subject }}</td>
                                        <td>{{ $row->created_at }}</td>
                                        <td><a href="/admin/show-message/{{ $row->id }}"><span class="glyphicon glyphicon-eye-open" title="Show Message"></span></a></td>
                                    </tr>
                                @endforeach
                                @endif
                                </tbody>
                            </table>
                            {{ $messages->links() }}
                        </div>
                    </div>
                </div>
            </div>

        </div>
        </div>
    </div>
@endsection
