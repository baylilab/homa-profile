@extends("admin.main")
@section('pages', $pages)
@section('title', $title)
@section("edit-news")
<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-head-line">Edit News</h4>

            </div>

        </div>
        <div class="row">
            <div class="col-md-12">

                    <div class="panel panel-default">
                            <div class="panel-heading">
                                @if(Session::has('success'))  
                                <div class="alert alert-success">
                                  {{ Session::get('success')}} 
                                </div>
                                @endif
                            </div>
                            <div class="panel-body">
                                <form role="form" action="/admin/update-news/{{ $news->id }}" method="POST" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                            <div class="form-group has-success">
                                                <label class="control-label" for="success">Title</label>
                                                <input type="text" class="form-control" id="primary" value="{{ $news->title }}" name="title" />
                                            </div>
                                            <div class="form-group has-success">
                                                    <label class="control-label" for="success">Image (Image resolution must 750x300 pixel)</label>
                                                    <input type="file" class="form-control" id="primary" name="images" />
                                             </div>
                                             <div class="form-group has-success">
                                                    <label class="control-label" for="success">News</label>
                                                    <textarea class="form-control" name="news" id="news">{{ $news->news }}</textarea>
                                                    
                                             </div>
                                             <div class="form-group has-success">
                                                    <button type="submit" class="btn btn-md btn-default">Edit News</button>
                                             </div>
                                        </form>
                                        @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            </div>
            </div>

        </div>
        </div>
    </div>
@endsection
