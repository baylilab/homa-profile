@extends("admin.main")
@section('pages', $pages)
@section('title', $title)
@section("show-messages")
<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-head-line">Master Messages</h4>

            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                       
                    <div class="panel-heading">
                    </div>
                    
                    <div class="panel-body">
                        <div class="table-responsive">
                        <label for="">From :</label><span>{{ $messages->name }}</span><br>
                        <label for="">Email :</label><span>{{ $messages->email }}</span><br>
                        <label for="">Subject :</label><span>{{ $messages->subject }}</span><br>
                                <p>{{ $messages->message }}</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        </div>
    </div>
@endsection
