@extends("admin.main")
@section('pages', $pages)
@section('title', $title)
@section("master-news")
<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-head-line">Master News</h4>

            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                    <a href="/admin/add-news/" class="col-md-offset-10" style="padding-left:10px"><label >New POST</label> <span class="glyphicon glyphicon-plus" title="Add New POST"></span></a>
                <div class="panel panel-default">
                       
                    <div class="panel-heading">
                        @if(Session::has('deleteSuccess'))  
                                <div class="alert alert-success">
                                  {{ Session::get('deleteSuccess')}} 
                                </div>
                                @endif
                    </div>
                    
                    <div class="panel-body">
                        <div class="table-responsive">
                                
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Title</th>
                                        <th>Image</th>
                                        
                                        <th>News Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($news) == 0)
                                        <h3>No Data Record Found</h3>
                                    @else
                                    @foreach($news as $row)
                                    <tr class="success">
                                        <td>{{ $row->id }}</td>
                                        <td>{{ $row->title }}</td>
                                        <td>{{ $row->images }}</td>
                                        <td>{{ $row->created_at }}</td>
                                        <td><a href="/admin/edit-news/{{ $row->id }}"><span class="glyphicon glyphicon-pencil" title="Edit"></span></a>&nbsp;&nbsp;<a href="/admin/delete-news/{{ $row->id }}" ><span class="glyphicon glyphicon-remove" title="Delete"></span></a></td>
                                    </tr>
                                @endforeach
                                @endif
                                </tbody>
                            </table>
                            {{ $news->links() }}
                        </div>
                    </div>
                </div>
            </div>

        </div>
        </div>
    </div>
@endsection
