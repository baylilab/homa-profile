<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class News extends Model
{
    protected $table = 'news';
    public $primary = 'id';
   
    protected $fillable = [
        'id','title', 'images', 'news'
    ];
}
