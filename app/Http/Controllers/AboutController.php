<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function index()
    {
        $pages = "about";
        $title = "About Profile - Pt. Homa Sejahtera";
        return view("front/pages.about", compact('pages', 'title'));
    }
}
