<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use DB;

class NewsController extends Controller
{
    public function index()
    {
        $pages = "news";
        $title = "News";

        $news = DB::table("news")->orderBy('created_at','DESC')->paginate(5);
        return view("front/pages.news", compact('pages', 'title','news'));
      
    }

    public function detailNews($id)
    {
        $pages = "detail-news";
        $title = "News";
        $news = News::find($id);
        return view("front/pages.detail_news", compact('pages', 'title','news'));
    }
}
