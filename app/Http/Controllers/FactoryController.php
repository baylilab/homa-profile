<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FactoryController extends Controller
{
    public function index()
    {
        $pages = "factory";
        $title = "Our factory";
        return view("front/pages.factory", compact('pages', 'title'));
    }
}
