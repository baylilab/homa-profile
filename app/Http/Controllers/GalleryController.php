<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GalleryController extends Controller
{
    public function index()
    {
        $pages = "gallery";
        $title = "Gallery";
        return view("front/pages.galery", compact('pages', 'title'));
    }
}
