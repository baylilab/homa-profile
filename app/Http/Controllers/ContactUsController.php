<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Messages;

class ContactUsController extends Controller
{
    public function index()
    {
        $pages = "contact-us";
        $title = "Our Contact";
        return view("front/pages.contact_us", compact('pages', 'title'));
    }

    public function sendMessage(Request $request)
    {
        $this->validate($request, [

               'name' => 'required',
               'email' => 'required|email',
               'subject' => 'required',
               'message' => 'required'

        ]);

       

            $messages = new Messages;

            $messages->name = $request->name;
            $messages->email = $request->email;
            $messages->subject = $request->subject;
            $messages->message = $request->message;
            $messages->save();

            return redirect("/contactUs")->with("success", "your message has been received, and we will follow up immediately ");
           
        }
        
    
}
