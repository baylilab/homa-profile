<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class MainController extends Controller
{
    public function index()
    {
        $pages = "home";
        $title = "Home - Pt. Homa Sejahtera";
        $news  = DB::table("news")->orderBy("created_at", 'DESC')->limit(2)->get(); 
        return view("front/pages.home", compact('pages', 'title', 'news'));
    }
}
