<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Messages;

class MessagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $pages = "master-messages";
        $title = "Master Messages";
        $messages = Messages::paginate(10);
        return view("admin/pages.master_messages", compact('pages', 'title','messages'));
    }

    public function showMessage($id)
    {
        $messages = Messages::find($id);
        $pages = "show-messages";
        $title = "Showing Message";
        return view("admin/pages.show_message", compact('pages', 'title','messages'));
    }
}
