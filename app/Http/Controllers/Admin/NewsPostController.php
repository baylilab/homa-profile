<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\News;

class NewsPostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $pages = "admin-dasboard";
        $title = "Welcome To Dasboard";
        return view("admin/pages.dasboard", compact('pages', 'title'));
    }

    public function masterNews()
    {
        $pages = "master-news";
        $title = "Welcome To Dasboard";
        $news  = News::paginate(5);
        return view("admin/pages.master_news", compact('pages', 'title','news'));
    }

    public function formNews()
    {
        $pages = "form-news";
        $title = "Form Add New Post";
        return view("admin/pages.form_news", compact('pages', 'title'));
    }

    public function publishNews(Request $request)
    {
        $this->validate($request, [

            'title' => 'required',
            'images' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:5000',
            'news' => 'required'
        ]);

        
        $publish = new News;
        $getimageName = time().'.'.$request->images->getClientOriginalExtension();
        $request->images->move(public_path('asset/image_news'), $getimageName);

        $publish->title = $request->title;
        $publish->images = $getimageName;
        $publish->news = $request->news;
        $publish->save();
        return redirect("/admin/add-news")->with("success", "Publish success");
    }

    public function deleteNews($id)
    {
        $news = News::find($id);

        $news->delete();

        return back()->with('deleteSuccess', 'News has deleted');
    }

    public function formEditNews($id)
    {
        $pages = "edit-news";
        $title = "Form Edit Post";

        $news = News::find($id);

        return view("admin/pages.form_edit_news", compact('pages', 'title','news'));
    }

    public function editNews(Request $request, $id)
    {
        $this->validate($request, [

            'title' => 'required',
            'images' => 'image|mimes:jpg,png,jpeg,gif,svg|max:5000',
            'news' => 'required'
        ]);

        $publish = new News;

        if($request->hasFile('images')) {

            $publish = News::find($id);
            $getimageName = time().'.'.$request->images->getClientOriginalExtension();
            $request->images->move(public_path('asset/image_news'), $getimageName);

            $publish->title = $request->title;
            $publish->images = $getimageName;
            $publish->news = $request->news;
            $publish->save();
            return redirect("/admin/add-news")->with("success", "Update success");

        } else {

            $publish = News::find($id);
            $publish->title = $request->title;
            $publish->news = $request->news;
            $publish->save();
            return redirect("/admin/add-news")->with("success", "Update success");
        }
        
       
    }
}
