<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CostumerProfileController extends Controller
{
    public function index()
    {
        $pages = "costumer-profile";
        $title = "costumer-profile";
        return view("front/pages.costumer_profile", compact('pages', 'title'));
    }
}
